For development and building the application been used IntelliJ IDE and Spring Boot with Java 11.
There was used Git CVS for the project, so it is possible to clone the project, open with IntelliJ IDE and run.
Cloning the test project: 'git clone https://bumerang83@bitbucket.org/bumerang83/swedbanktest_03_08_2020.git'

task1 - been created task1 branch for the task.
building the task1: using IntellJ terminal run 'mvn install'
running the application: being in the 'tasks' folder run 'java -jar target/tasks-0.0.1-SNAPSHOT.jar [output.txt]'
the results are in the 'tasks/output.txt' file, the output file could be defined in the prompt while
running the application and have that defined name

task2 - been created task2 branch for the task.
building the task2: using IntellJ terminal run 'mvn install'
running the application: being in the 'tasks' folder run 'java -jar target/tasks-0.0.1-SNAPSHOT.jar [output.txt]'
the results are in the 'tasks/output.txt' file, the output file could be defined in the prompt while
running the application and have that defined name

task3 - open project with IntelliJ IDE, switch the task3 branch
and run the project. All results are in the H2 database. Open the 
database with URL: localhost:8081/h2-console
user name: sa, user password: (blank)
JDBC URL: jdbc:h2:mem:swedbanktest
PLEASE CHECK RESULTS IN THE CASCO_FEE TABLE
